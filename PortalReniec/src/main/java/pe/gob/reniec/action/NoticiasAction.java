/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.reniec.action;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.struts2.interceptor.RequestAware;
import pe.gob.reniec.domain.NoticiasBean;
import pe.gob.reniec.util.UtilPortal;

/**
 *
 * @author HP
 */
public class NoticiasAction extends ActionSupport implements RequestAware,Preparable{

    private Map<String,Object> request;
    private NoticiasBean noticiaBean;
    private List<NoticiasBean> listadoNoticias;
    private List<JournalArticle> l;
    private Map<String,JournalArticle> lista;
    private Document document;

    @Override
    public void setRequest(Map<String, Object> request) {
        this.request = request;
    }

    @Override
    public void prepare() throws Exception {
        noticiaBean = new NoticiasBean();
        listadoNoticias = new ArrayList<NoticiasBean>();
        lista = new HashMap<String, JournalArticle>();
        ThemeDisplay themeDisplay = (ThemeDisplay) request.get(WebKeys.THEME_DISPLAY);
        long portletGroupId = themeDisplay.getPortletGroupId();
        l = JournalArticleLocalServiceUtil.getStructureArticles(portletGroupId, "11508");

    }

    public String listarNoticias()throws Exception{
        if(l != null && l.size() > 0){
            for(JournalArticle journal: l){
                if(lista.get(journal.getArticleId()) == null){
                    lista.put(journal.getArticleId(), journal);
                    document = SAXReaderUtil.read(journal.getContent());

                    NoticiasBean noticia = new NoticiasBean();
                    noticia.setJournal(journal);
                    noticia.setTitulo(UtilPortal.getSingleNodeXML(document, "titulo"));
                    noticia.setFoto(UtilPortal.getSingleNodeXML(document, "imagen"));
                    noticia.setPublicador(UtilPortal.getSingleNodeXML(document, "publicador"));
                    noticia.setCuerpo(UtilPortal.getSingleNodeXML(document, "cuerpo"));
                    
                    listadoNoticias.add(noticia);
                }
            }
        }
        return SUCCESS;
    }

    public Map<String, JournalArticle> getLista() {
        return lista;
    }

    public void setLista(Map<String, JournalArticle> lista) {
        this.lista = lista;
    }

    public List<NoticiasBean> getListadoNoticias() {
        return listadoNoticias;
    }

    public void setListadoNoticias(List<NoticiasBean> listadoNoticias) {
        this.listadoNoticias = listadoNoticias;
    }

    public NoticiasBean getNoticiaBean() {
        return noticiaBean;
    }

    public void setNoticiaBean(NoticiasBean noticiaBean) {
        this.noticiaBean = noticiaBean;
    }

    
}
