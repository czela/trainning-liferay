/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.reniec.util;

import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;

/**
 *
 * @author HP
 */
public class UtilPortal {

    public static String getStringXML(String campo) {
        return "/root/dynamic-element[@name='" + campo + "']/dynamic-content";
    }

    public static String getSingleNodeXML(Document document, String strNodo) {
        Node node = document.selectSingleNode(UtilPortal.getStringXML(strNodo));
        if (node == null) {
            return "";
        } else {
            return node.getText();
        }
    }

}
