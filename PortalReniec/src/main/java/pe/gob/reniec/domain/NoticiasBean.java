/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.reniec.domain;

import com.liferay.portlet.journal.model.JournalArticle;
import java.io.Serializable;

/**
 *
 * @author czela
 */
public class NoticiasBean implements Serializable{

    private JournalArticle journal;
    private String titulo;
    private String publicador;
    private String foto;
    private String cuerpo;

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public JournalArticle getJournal() {
        return journal;
    }

    public void setJournal(JournalArticle journal) {
        this.journal = journal;
    }

    public String getPublicador() {
        return publicador;
    }

    public void setPublicador(String publicador) {
        this.publicador = publicador;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }



}
