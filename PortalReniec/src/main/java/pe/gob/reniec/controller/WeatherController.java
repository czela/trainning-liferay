/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.reniec.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pe.gob.reniec.service.WeatherSerice;

/**
 *
 * @author czela
 */

@Controller
@RequestMapping("VIEW")
public class WeatherController {

    private WeatherSerice weatherService;
    
    @Autowired
    public void WeatherController(WeatherSerice weatherService) {
        this.weatherService = weatherService;
    }
    @RequestMapping
    public String handleRenderRequestInternal(Model model) throws Exception{
        model.addAttribute("temperatures",weatherService.getMajorCityTemperatures());
        return "weatherView";
    }

    
}
