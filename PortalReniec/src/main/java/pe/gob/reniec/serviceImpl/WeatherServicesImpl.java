/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pe.gob.reniec.serviceImpl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import pe.gob.reniec.service.WeatherSerice;

/**
 *
 * @author czela
 */
public class WeatherServicesImpl implements WeatherSerice{

    @Override
    public Map<String, Double> getMajorCityTemperatures() {
        Map<String,Double> temperatures = new HashMap<String, Double>();
        temperatures.put("New York", 6.0);
        temperatures.put("London", 10.0);
        temperatures.put("Lima", 5.0);
        return temperatures;
    }

}
