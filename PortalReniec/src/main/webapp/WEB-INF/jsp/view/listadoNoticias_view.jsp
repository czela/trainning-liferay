<%-- 
    Document   : listadoNoticias_view
    Created on : 22/08/2011, 07:03:08 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
    <table>
        <tbody>
            <s:iterator value="listadoNoticias" var="noticia">
            <tr>
                <td><s:property value="#noticia.titulo"/></td>
                <td><s:property value="#noticia.publicador"/></td>
                <td><img src="<s:property value="#noticia.foto"/>"/></td>
                <td><s:property value="#noticia.cuerpo" escape="false"/></td>
            </tr>
            </s:iterator>
        </tbody>
    </table>
</html>
